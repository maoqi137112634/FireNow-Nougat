package com.android.settings;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.SystemClock;
import android.util.Log;
import android.os.SystemProperties; 

public class BootReceiver extends BroadcastReceiver{

  @Override
  public void onReceive(Context arg0, Intent arg1) {

      String action = arg1.getAction();
      Log.i("sjfa","action:"+action);   
      if(action.equals(Intent.ACTION_BOOT_COMPLETED)){ 
          SharedPreferences shared = arg0.getSharedPreferences("com.android.settings_preferences", Context.MODE_PRIVATE);
                 boolean enable_log_save = shared.getBoolean("enable_log_save", false);
          String  persist_sys_cat_log =SystemProperties.get("persist.sys.cat_log");
                 Log.i("BootReceiver","action==ACTION_BOOT_COMPLETED,BootReceiver is start");          
                 if(persist_sys_cat_log.equals("1")){
                    SystemProperties.set("persist.sys.cat_log","1");   
                 }    
      }else if("android.fireflyapi.action.run_power_off".equals(action)){
        arg0.startActivity(new Intent("android.fireflyapi.action_request_shutdown"));
      }
  }

}
